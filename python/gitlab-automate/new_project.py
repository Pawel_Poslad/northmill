from atlassian import Bitbucket
import gitlab
import boto3
import base64
import argparse
import json

def get_secret(secret_id):
    sm = boto3.client('secretsmanager')
    res = sm.get_secret_value(SecretId=secret_id)
    if 'SecretString' in res:
        return res['SecretString']
    return base64.b64decode(res['SecretBinary'])


sa_username = "sa_username"
bitbucket_url = "bitbucket_url"
bitbucket_token = get_secret(secret_id="SA-Bitbucket-Token")
gitlab_url = "gitlab_url"
gitlab_token = get_secret(secret_id="SA-Gitlab-Token")
bitbucket_project_key = "bitbucket_project_key"


bt = Bitbucket(
    url=bitbucket_url,
    token=bitbucket_token
)

gl = gitlab.Gitlab(
    url=gitlab_url,
    private_token=gitlab_token,
    ssl_verify="./KN_Root_Ca.pem"
)


parser = argparse.ArgumentParser()
parser.add_argument("--repo_name", required=True, help="Repository name")
parser.add_argument("--group_name", required=True, help="GitLab group name")
parser.add_argument("--ecr_names", nargs='*', help="Space-separated list of repository names")
args = parser.parse_args()

repo_name = args.repo_name
group_name = args.group_name
group_id = gl.groups.list(search=group_name)[0].id


def create_gitlab_repository():
    try:
        gl.auth()
        project = gl.projects.create(
            {
                'name': repo_name,
                'namespace_id': group_id,
                'visibility': 'private',
                'default_branch': 'master',
            }
        )
        print(f"Repository '{repo_name}' was created successfully on GitLab!")
        return project
    except gitlab.GitlabAuthenticationError:
        print("Authentication failed on GitLab. Please check if you provided the correct GitLab URL and private token.")
        return None
    except gitlab.GitlabCreateError as e:
        print(f"An error occurred while creating the repository on GitLab: {e}")
        return None


def create_bitbucket_repository():
    try:
        response = bt.create_repo(project_key=bitbucket_project_key, repository_slug=repo_name, is_private=True)
        clone_links = response['links']['clone']
        for link in clone_links:
            if link['name'] == 'http':
                http_clone_link = link['href']
        bitbucket_mirror_repo_url = http_clone_link.replace("https://", f"https://{sa_username}:{bitbucket_token}@")
        print(f"Repository '{repo_name}' was created successfully on Bitbucket!")
        return response, bitbucket_mirror_repo_url
    except Exception as e:
        print(f"An error occurred while creating the repository on Bitbucket: {e}")
        return None, None


def set_branches_permissions(bitbucket_repo):
    try:
        bt.set_branches_permissions(
            project_key=bitbucket_project_key,
            matcher_type="pattern",
            matcher_value="*",
            permission_type="read-only",
            repository_slug=repo_name,
            except_users=[sa_username],
        )
        print(f"Permissions for the repo '{repo_name}' were set on Bitbucket!")
    except Exception as e:
        print(f"An error occurred while setting permissions for the branch on Bitbucket: {e}")


def add_push_mirror_to_gitlab(project, bitbucket_mirror_repo_url):
    gl_remote_url = project.http_url_to_repo

    try:
        mirror = project.remote_mirrors.create({
            'url': bitbucket_mirror_repo_url,
            'enabled': True,
            'only_mirror_protected_branches': False
        })
        print(f"Push mirror was added to repository {repo_name} on Gitlab, sending code to Bitbucket!")
    except gitlab.GitlabCreateError as e:
        print(f"An error occurred while adding the push mirror to the GitLab repository: {e}")

def get_aws_account_numbers(group_name):
    ssm = boto3.client("ssm")

    aws_account_dev_param = f"/cicd/{group_name}/account/dev"
    aws_account_prod_param = f"/cicd/{group_name}/account/prod"

    try:
        aws_account_dev = ssm.get_parameter(Name=aws_account_dev_param)['Parameter']['Value']
        aws_account_prod = ssm.get_parameter(Name=aws_account_prod_param)['Parameter']['Value']
        return aws_account_dev, aws_account_prod
    except ssm.exceptions.ParameterNotFound:
        return None, None

def create_ecr_repositories(ecr_names, aws_account_dev, aws_account_prod):
    ecr_client = boto3.client("ecr")

    for ecr_name in ecr_names:
        try:
            ecr_client.create_repository(repositoryName=ecr_name)
            print(f"ECR repository '{ecr_name}' created successfully.")

            repository_uri = f"{ecr_client.meta.endpoint_url}/{ecr_name}"

            ecr_policy = {
                "Version": "2008-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Principal": {
                            "AWS": [
                                f"arn:aws:iam::{aws_account_dev}:root",
                                f"arn:aws:iam::{aws_account_prod}:root"
                            ]
                        },
                        "Action": [
                            "ecr:BatchCheckLayerAvailability",
                            "ecr:BatchGetImage",
                            "ecr:GetDownloadUrlForLayer"
                        ]
                    },
                    {
                        "Effect": "Allow",
                        "Principal": {
                            "Service": "lambda.amazonaws.com"
                        },
                        "Action": [
                            "ecr:BatchCheckLayerAvailability",
                            "ecr:BatchGetImage",
                            "ecr:GetDownloadUrlForLayer"
                        ],
                        "Condition": {
                            "StringLike": {
                                "aws:sourceARN": [
                                    f"arn:aws:lambda:eu-central-1:{aws_account_dev}:function:*",
                                    f"arn:aws:lambda:eu-central-1:{aws_account_prod}:function:*"
                                ]
                            }
                        }
                    }
                ]
            }

            lifecycle_policy = {
                "rules": [
                    {
                        "rulePriority": 1,
                        "description": "Keep last 5 PROD images",
                        "selection": {
                            "tagStatus": "tagged",
                            "tagPrefixList": ["prod"],
                            "countType": "imageCountMoreThan",
                            "countNumber": 5
                        },
                        "action": {
                            "type": "expire"
                        }
                    },
                    {
                        "rulePriority": 2,
                        "description": "Keep last 3 DEV images",
                        "selection": {
                            "tagStatus": "tagged",
                            "tagPrefixList": ["dev"],
                            "countType": "imageCountMoreThan",
                            "countNumber": 3
                        },
                        "action": {
                            "type": "expire"
                        }
                    },
                    {
                        "rulePriority": 3,
                        "description": "Keep images not older than 1 week",
                        "selection": {
                            "tagStatus": "any",
                            "countType": "sinceImagePushed",
                            "countUnit": "days",
                            "countNumber": 7
                        },
                        "action": {
                            "type": "expire"
                        }
                    }
                ]
            }
            response = ecr_client.set_repository_policy(
                repositoryName=ecr_name,
                policyText=json.dumps(ecr_policy)
            )
            response = ecr_client.put_lifecycle_policy(
                repositoryName=ecr_name,
                lifecyclePolicyText=json.dumps(lifecycle_policy)
            )

            print(f"ECR policy added to ECR repository '{ecr_name}' successfully.")
        except ecr_client.exceptions.RepositoryAlreadyExistsException:
            print(f"ECR repository '{ecr_name}' already exists.")
        except Exception as e:
            print(f"An error occurred while creating ECR repository '{ecr_name}': {e}")


if __name__ == "__main__":
    gitlab_project = create_gitlab_repository()
    if gitlab_project:
        bitbucket_repo, bitbucket_mirror_repo_url = create_bitbucket_repository()
        if bitbucket_mirror_repo_url:
            add_push_mirror_to_gitlab(gitlab_project, bitbucket_mirror_repo_url)
            set_branches_permissions(bitbucket_repo)
            aws_account_dev, aws_account_prod = get_aws_account_numbers(group_name)
            if args.ecr_names:
                create_ecr_repositories(args.ecr_names, aws_account_dev, aws_account_prod)
            else:
                print("No ECR repository names provided. Skipping ECR repository creation.")
