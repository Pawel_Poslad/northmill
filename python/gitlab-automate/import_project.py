from atlassian import Bitbucket
import gitlab
import boto3
import base64
import time
import argparse


def get_secret(secret_id):
    sm = boto3.client('secretsmanager')
    res = sm.get_secret_value(SecretId=secret_id)
    if 'SecretString' in res:
        return res['SecretString']
    return base64.b64decode(res['SecretBinary'])


sa_username = "sa_username"
bitbucket_url = "bitbucket_url"
bitbucket_token = get_secret(secret_id="SA-Bitbucket-Token")
gitlab_url = "gitlab_url"
gitlab_token = get_secret(secret_id="SA-Gitlab-Token")
bitbucket_project_key = "bitbucket_project_key"


bt = Bitbucket(
    url=bitbucket_url,
    token=bitbucket_token
)

gl = gitlab.Gitlab(
    url=gitlab_url,
    private_token=gitlab_token,
    ssl_verify="./CA.pem"
)

parser = argparse.ArgumentParser()
parser.add_argument("--repo_name", required=True, help="Repository name")
parser.add_argument("--group_name", required=True, help="GitLab group name")
args = parser.parse_args()

repo_name = args.repo_name
group_name = args.group_name
group_id = gl.groups.list(search=group_name)[0].id


def import_bitbucket_repo_to_gitlab():
    try:
        print("Triggering import")
        gl.auth()
        result = gl.projects.import_bitbucket_server(
            bitbucket_server_url=bitbucket_url,
            bitbucket_server_username=sa_username,
            personal_access_token=bitbucket_token,
            bitbucket_server_project=bitbucket_project_key,
            bitbucket_server_repo=repo_name,
            new_name=repo_name,
            target_namespace=f"{bitbucket_project_key}/{group_name}"
        )

        import_status = result.get('import_status')

        while import_status != 'finished':
            time.sleep(1)
            project = gl.projects.get(result['id'])
            import_status = project.import_status
            print(import_status)

        print("Repository has been imported to Gitlab!")
        return project
    except gitlab.GitlabAuthenticationError:
        print("Authentication failed on GitLab. Please check if you provided the correct GitLab URL and private token.")
        return None
    except gitlab.GitlabCreateError as e:
        print(f"An error occurred while creating the repository on GitLab: {e}")
        return None


def get_bitbucket_repo():
    try:
        bitbucket_repo = bt.get_repo(
            project_key=bitbucket_project_key,
            repository_slug=repo_name
        )
        clone_links = bitbucket_repo['links']['clone']
        for link in clone_links:
            if link['name'] == 'http':
                http_clone_link = link['href']
        bitbucket_mirror_repo_url = http_clone_link.replace("https://", f"https://{sa_username}:{bitbucket_token}@")
        print(f"Repository '{repo_name}' was getted successfully on Bitbucket!")
        return bitbucket_repo, bitbucket_mirror_repo_url
    except Exception as e:
        print(f"An error occurred while getting repository on Bitbucket: {e}")


def set_branches_permissions(bitbucket_repo):
    try:
        bt.set_branches_permissions(
            project_key=bitbucket_project_key,
            matcher_type="pattern",
            matcher_value="*",
            permission_type="read-only",
            repository_slug=repo_name,
            except_users=[sa_username],
        )
        print(f"Permissions for the repo '{repo_name}' were set on Bitbucket!")
    except Exception as e:
        print(f"An error occurred while setting permissions for the branch on Bitbucket: {e}")


def add_push_mirror_to_gitlab(project, bitbucket_mirror_repo_url):
    gl_remote_url = project.http_url_to_repo

    try:
        mirror = project.remote_mirrors.create({
            'url': bitbucket_mirror_repo_url,
            'enabled': True,
            'only_mirror_protected_branches': False
        })
        print(f"Push mirror was added to repository {repo_name} on Gitlab, sending code to Bitbucket!")
    except gitlab.GitlabCreateError as e:
        print(f"An error occurred while adding the push mirror to the GitLab repository: {e}")


if __name__ == "__main__":
    gitlab_project = import_bitbucket_repo_to_gitlab()
    if gitlab_project:
        bitbucket_repo, bitbucket_mirror_repo_url = get_bitbucket_repo()
        if bitbucket_mirror_repo_url:
            add_push_mirror_to_gitlab(gitlab_project, bitbucket_mirror_repo_url)
            set_branches_permissions(bitbucket_repo)
