import os
import subprocess

DOCKER_REGISTRY = "${aws_account}.dkr.ecr.${aws_region}.amazonaws.com/golden-images"

images_directory = "./images"

for category in os.listdir(images_directory):
    category_path = os.path.join(images_directory, category)

    if os.path.isdir(category_path):
        for version in os.listdir(category_path):
            version_path = os.path.join(category_path, version)

            if os.path.isdir(version_path):
                image_name = f"{category}-{version}"

                print(f"Building and pushing image for {image_name}")

                build_command = f"docker build -t {DOCKER_REGISTRY}:{image_name} ."
                push_command = f"docker push {DOCKER_REGISTRY}:{image_name}"

                try:
                    subprocess.run(build_command, shell=True, cwd=version_path, check=True)

                    subprocess.run(push_command, shell=True, check=True)

                except subprocess.CalledProcessError as e:
                    print(f"An error occurred while building/pushing the image for {image_name}: {e}")
