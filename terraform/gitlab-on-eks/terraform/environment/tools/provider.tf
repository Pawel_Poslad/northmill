terraform {
  required_version = ">= 1.1"

  required_providers {
    aws = {
      version = ">= 3.69.0"
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile = var.aws.profile
  region  = var.aws.region
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = module.eks_cluster.arn
}

provider "helm" {
  kubernetes {
    config_path    = "~/.kube/config"
    config_context = module.eks_cluster.arn
  }
}

terraform {
  backend "s3" {
    bucket         = "tf-state"
    key            = "key.tfstate"
    dynamodb_table = "dynamodbLockTable"
    profile        = "profile"
    region         = "aws_region"
  }
}