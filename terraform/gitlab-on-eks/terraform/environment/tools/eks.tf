module "eks_iam" {
  #General
  source       = "../../modules/eks_roles"
  common_tags  = var.common_tags
  region       = var.aws.region
  cluster_name = "eks-cluster"
  kms_key_arn  = "*"
}

module "eks_cidr" {
  source              = "../../modules/eks_cidr"
  common_tags         = {}
  eks_cluster_name    = module.eks_cluster.name
  eks_az1             = module.network.availability_zones[0]
  eks_az2             = module.network.availability_zones[1]
  eks_az3             = module.network.availability_zones[2]
  eks_cidr_subnet_id1 = module.network.eks_subnet_ids[0]
  eks_cidr_subnet_id2 = module.network.eks_subnet_ids[1]
  eks_cidr_subnet_id3 = module.network.eks_subnet_ids[2]
}

module "eks_cluster" {
  #General
  source                    = "../../modules/eks"
  common_tags               = var.common_tags
  depends_on                = [module.eks_iam]
  cluster_name              = "eks-cluster"
  cluster_role_arn          = module.eks_iam.cluster_role_arn
  cluster_version           = "1.24"
  cloudwatch_logs_retention = 30
  cluster_log_types         = ["audit", "api"]
  security_group_ids        = [module.network.security_groups["eks"].id]
  cluster_subnet_ids        = module.network.private_subnet_ids
}

module "eks_node" {
  source               = "../../modules/eks_node"
  common_tags          = var.common_tags
  cluster_name         = module.eks_cluster.name
  api_endpoint         = module.eks_cluster.endpoint
  ca_certificate       = module.eks_cluster.ca_certificate
  capacity_type        = "ON_DEMAND"
  nodes_min_count      = 1
  nodes_max_count      = 3
  nodes_desired_count  = 2
  launch_template_name = "launch-template"
  base_ami_image       = "ami-09ba4b00d041bf284"
  proxy                = "proxy"
  no_proxy             = "localhost"
  cluster_subnet_ids   = module.network.private_subnet_ids
  instance_types       = ["m5.xlarge"]
  node_group_name      = "node-group"
  worker_role_arn      = module.eks_iam.worker_role_arn
  vpc_cidr             = "10.0.0.0/24,100.64.0.0/16"
}
