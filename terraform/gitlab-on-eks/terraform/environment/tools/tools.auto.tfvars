# General
aws = {
  profile = "profile"
  region  = "aws_region"
}

common_tags = {
  environment = "environment"
  maintainer  = "maintainer"
  iac         = "true"
  managed     = "terraform"
}

# Network
vpc_cidr = "10.0.0.0/24"

vpc_secondary_cidr = "100.64.0.0/16"

private_subnets_cidrs = [
  "10.0.0.0/26",
  "10.0.0.1/26",
  "10.0.0.2/26"
]

eks_subnets_cidrs = [
  "100.64.0.0/18",
  "100.64.64.0/18",
  "100.64.128.0/18"
]
availability_zones = 3

# Security Groups
security_groups = {
  "vpc-endpoint" = {
    "ecr" = { from_port = 443, to_port = 443, protocol = "tcp", source = "0.0.0.0/0" }
  }
  "elasticache" = {
    "redis" = { from_port = 6379, to_port = 6379, protocol = "tcp", source = "0.0.0.0/0" }
  }
  "rds" = {
    "postgresql" = { from_port = 5432, to_port = 5432, protocol = "tcp", source = "0.0.0.0/0" }
  }
  "eks" = {
    "cluster-sg" = { from_port = 22, to_port = 30000, protocol = "tcp", source = "0.0.0.0/0" }
  }
}

#RDS
rds_allocated_storage               = 110
rds_engine                          = "postgres"
rds_engine_version                  = "14.7"
rds_db_type                         = "db.m5.xlarge"
rds_multi_az                        = "false"
rds_storage_type                    = "gp2"
rds_skip_final_snapshot             = true
rds_apply_immediately               = true
rds_db_name                         = "gitlab_primary_db"
rds_username                        = "gitlab_primary_db_admin"
rds_backup_retention_period         = 7
rds_backup_window                   = "02:00-03:00"
rds_encryption                      = true
rds_enabled_cloudwatch_logs_exports = ["postgresql"]

#Elasticache
elasticache_engine          = "redis"
elasticache_engine_version  = "7.0"
elasticache_node_type       = "cache.m4.xlarge"
elasticache_num_cache_nodes = 1

#EBS
ebs_size      = 200
ebs_type      = "gp2"
ebs_encrypted = true