module "rds" {
  #General
  source      = "../../modules/rds"
  common_tags = var.common_tags

  rds_name                            = ["gitlab-primary", "gitlab-replica"]
  availability_zones                  = var.availability_zones
  databases_subnets_ids               = module.network.private_subnet_ids
  rds_allocated_storage               = var.rds_allocated_storage
  rds_engine                          = var.rds_engine
  rds_engine_version                  = var.rds_engine_version
  rds_db_type                         = var.rds_db_type
  rds_multi_az                        = var.rds_multi_az
  rds_storage_type                    = var.rds_storage_type
  rds_skip_final_snapshot             = var.rds_skip_final_snapshot
  rds_apply_immediately               = var.rds_apply_immediately
  rds_enabled_cloudwatch_logs_exports = var.rds_enabled_cloudwatch_logs_exports
  rds_db_name                         = var.rds_db_name
  rds_username                        = var.rds_username
  rds_sg_ids                          = [module.network.security_groups["rds"].id]
  rds_backup_retention_period         = var.rds_backup_retention_period
  rds_backup_window                   = var.rds_backup_window
  rds_encryption                      = var.rds_encryption
}