module "network" {
  #General
  source      = "../../modules/network"
  common_tags = var.common_tags
  region      = var.aws.region

  #VPC
  vpc_cidr           = var.vpc_cidr
  vpc_secondary_cidr = var.vpc_secondary_cidr

  #Subnets
  private_subnets_cidrs = var.private_subnets_cidrs
  availability_zones    = var.availability_zones
  eks_subnets_cidrs     = var.eks_subnets_cidrs
  # SG
  security_groups = var.security_groups

}