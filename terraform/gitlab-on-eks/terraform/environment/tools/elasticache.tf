module "elasticache" {
  #General
  source                      = "../../modules/elasticache"
  common_tags                 = var.common_tags
  databases_subnets_ids       = module.network.private_subnet_ids
  elasticache_engine          = var.elasticache_engine
  elasticache_engine_version  = var.elasticache_engine_version
  elasticache_node_type       = var.elasticache_node_type
  elasticache_num_cache_nodes = var.elasticache_num_cache_nodes
  elasticache_sg_ids          = [module.network.security_groups["elasticache"].id]
}