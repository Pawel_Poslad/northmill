module "gitlab" {
  #General
  source                 = "../../modules/gitlab_chart"
  common_tags            = var.common_tags
  gitaly_ebs_volume_id   = module.ebs.gitaly_ebs_id
  gitlab_access_role_arn = module.eks_cluster.gitlab_access_role_arn
  rds_password           = module.rds.rds_password
  region                 = var.aws.region
  availability_zones     = module.network.availability_zones
  elasticache_host       = module.elasticache.elasticache_host
  gitlab_dns_name        = "gitlab"
  rds_host_primary       = module.rds.rds_host["gitlab-primary"].address
  rds_host_replica_0     = module.rds.rds_host["gitlab-replica"].address
  rds_port               = 5432
  s3_git_lfs_id          = module.s3_gitlab.s3_gitlab["git-lfs"].id
  s3_gitlab_artifacts_id = module.s3_gitlab.s3_gitlab["gitlab-artifacts"].id
  s3_gitlab_backups_id   = module.s3_gitlab.s3_gitlab["gitlab-backups"].id
  s3_gitlab_packages_id  = module.s3_gitlab.s3_gitlab["gitlab-packages"].id
  s3_gitlab_pseudo_id    = module.s3_gitlab.s3_gitlab["gitlab-pseudo"].id
  s3_gitlab_registry_id  = module.s3_gitlab.s3_gitlab["gitlab-registry"].id
  s3_gitlab_uploads_id   = module.s3_gitlab.s3_gitlab["gitlab-uploads"].id
  vpc_id                 = module.network.vpc_id
}