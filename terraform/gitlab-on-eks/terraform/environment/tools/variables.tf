# General
variable "aws" {
  type = map(any)
}

variable "common_tags" {
  type        = map(string)
  description = "tags configurations"
}

# VPC
variable "vpc_cidr" {
  type = string
}

variable "vpc_secondary_cidr" {
  type = string
}

variable "private_subnets_cidrs" {
  type = list(any)
}

variable "eks_subnets_cidrs" {
  type = list(any)
}

variable "availability_zones" {
  type = number
}

# SG
variable "security_groups" {
  type = map(any)
}

#RDS
variable "rds_allocated_storage" {
  type        = number
  description = "Allocated storage"
}

variable "rds_engine" {
  type        = string
  description = "Database engine"
}

variable "rds_engine_version" {
  type        = string
  description = "Database engine version"
}

variable "rds_db_type" {
  type        = string
  description = "Database type"
}

variable "rds_multi_az" {
  type        = bool
  description = "Multi AZ flag"
}

variable "rds_storage_type" {
  type        = string
  description = "Storage type"
}

variable "rds_skip_final_snapshot" {
  type        = bool
  description = "Skip final snapshot flag"
}

variable "rds_apply_immediately" {
  type        = bool
  description = "Apply immediately flag"
}

variable "rds_db_name" {
  type        = string
  description = "Database name"
}

variable "rds_username" {
  type        = string
  description = "Database username"
}

variable "rds_backup_retention_period" {
  type        = number
  description = "Backup retention period"
}

variable "rds_backup_window" {
  type        = string
  description = "Backup window"
}

variable "rds_encryption" {
  type        = bool
  description = "Encryption flag"
}

variable "rds_enabled_cloudwatch_logs_exports" {
  type        = list(string)
  description = "Enabled cloudwatch logs (list)"
}

#Elasticache
variable "elasticache_node_type" {
  type        = string
  description = "Node type"
}

variable "elasticache_num_cache_nodes" {
  type        = number
  description = "Number of nodes"
}

variable "elasticache_engine" {
  type        = string
  description = "Elasticache version"
}

variable "elasticache_engine_version" {
  type        = string
  description = "Elasticache version"
}

#EBS
variable "ebs_size" {
  type = number
}

variable "ebs_type" {
  type = string
}

variable "ebs_encrypted" {
  type = bool
}