module "s3_gitlab" {
  source      = "../../modules/s3"
  common_tags = var.common_tags
  s3_name     = ["gitlab-registry", "gitlab-runner-cache", "gitlab-backups", "gitlab-pseudo", "git-lfs", "gitlab-artifacts", "gitlab-uploads", "gitlab-packages"]
}