resource "aws_route_table" "private" {

  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    transit_gateway_id = "tgw"
  }

  tags = {
    Name = format("%s-private", lower(local.tag_environment))
  }
}

resource "aws_route_table_association" "private" {
  for_each = toset(local.availability_zones)

  subnet_id      = aws_subnet.private[each.key].id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table" "eks_cidr" {
  for_each = toset(local.availability_zones)

  vpc_id = aws_vpc.main.id

  tags = {
    Name = format("%s-eks-%s", lower(local.tag_environment), substr(each.key, -2, 2))
  }
}

resource "aws_route_table_association" "eks_cidr" {
  for_each = toset(local.availability_zones)

  subnet_id      = aws_subnet.eks_cidr[each.key].id
  route_table_id = aws_route_table.eks_cidr[each.key].id
}