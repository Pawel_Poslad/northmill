variable "common_tags" {
  type        = map(string)
  description = "tags configurations"
}

variable "region" {
  type        = string
  description = "target region"
}

# VPC
variable "vpc_cidr" {
  type = string
}

variable "vpc_secondary_cidr" {
  type = string
}

variable "private_subnets_cidrs" {
  type = list(any)
}

variable "eks_subnets_cidrs" {
  type = list(any)
}

variable "availability_zones" {
  type = number
}

# SG
variable "security_groups" {
  type = map(any)
}
