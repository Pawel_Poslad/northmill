resource "aws_vpc_endpoint" "ecr_dkr" {
  vpc_id              = aws_vpc.main.id
  private_dns_enabled = true
  vpc_endpoint_type   = "Interface"
  service_name        = "com.amazonaws.${var.region}.ecr.dkr"
  security_group_ids  = [aws_security_group.main["vpc-endpoint"].id]
  subnet_ids          = [for subnet in aws_subnet.private : subnet.id]
  tags = {
    Name = format("ecr-dkr-vpc-endpoint-%s", lower(local.tag_environment))
  }
}

resource "aws_vpc_endpoint" "ecr_api" {
  vpc_id              = aws_vpc.main.id
  private_dns_enabled = true
  vpc_endpoint_type   = "Interface"
  service_name        = "com.amazonaws.${var.region}.ecr.api"
  security_group_ids  = [aws_security_group.main["vpc-endpoint"].id]
  subnet_ids          = [for subnet in aws_subnet.private : subnet.id]
  tags = {
    Name = format("ecr-dkr-vpc-endpoint-%s", lower(local.tag_environment))
  }
}