resource "aws_subnet" "private" {
  for_each = toset(local.availability_zones)

  vpc_id            = aws_vpc.main.id
  availability_zone = each.key
  cidr_block        = var.private_subnets_cidrs[index(local.availability_zones, each.key)]

  tags = {
    Name = format("%s-private-%s", lower(local.tag_environment), substr(each.key, -2, 2))
  }
}

resource "aws_subnet" "eks_cidr" {
  for_each = toset(local.availability_zones)

  vpc_id            = aws_vpc.main.id
  availability_zone = each.key
  cidr_block        = var.eks_subnets_cidrs[index(local.availability_zones, each.key)]

  tags = {
    Name = format("%s-eks-%s", lower(local.tag_environment), substr(each.key, -2, 2))
  }
}