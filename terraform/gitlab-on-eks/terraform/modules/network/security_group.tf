resource "aws_security_group" "main" {
  for_each = var.security_groups

  name        = format("%s-%s", lower(local.tag_environment), each.key)
  description = format("%s-%s", lower(local.tag_environment), each.key)
  vpc_id      = aws_vpc.main.id

  tags = {
    Name = format("%s-%s", lower(local.tag_environment), each.key)
  }
}