output "vpc_id" {
  value = aws_vpc.main.id
}

output "private_subnet_ids" {
  value = [for subnet in aws_subnet.private : subnet.id]
}

output "private_subnet_ids_map" {
  value = {
    for az, subnet in aws_subnet.private : az => subnet.id
  }
}

output "eks_subnet_ids" {
  value = [for subnet in aws_subnet.eks_cidr : subnet.id]
}

output "eks_subnet_ids_map" {
  value = {
    for az, subnet in aws_subnet.eks_cidr : az => subnet.id
  }
}

output "security_groups" {
  value = aws_security_group.main
}

output "availability_zones" {
  value = local.availability_zones
}