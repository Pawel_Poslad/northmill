variable "cluster_name" {
  type        = string
  description = "EKS cluster name"
}

variable "cluster_subnet_ids" {
  type        = list(any)
  description = "List of subnets to EKS cluster to operate"
}

variable "common_tags" {
  type        = map(any)
  default     = {}
  description = "Tags map"
}

variable "security_group_ids" {
  type        = list(any)
  default     = []
  description = "EKS cluster additional security groups if any"
}

variable "operator_role_arn" {
  type    = string
  default = ""
}

variable "node_group_name" {
  type = string
}

variable "worker_role_arn" {
  type = string
}

variable "cluster_log_types" {
  type    = list(any)
  default = [] # ["api", "audit"]
}

variable "instance_types" {
  type = list(string)
}

variable "capacity_type" {
  type = string
}

variable "nodes_min_count" {
  type = string
}

variable "nodes_max_count" {
  type = string
}

variable "nodes_desired_count" {
  type = string
}

variable "launch_template_name" {
  type = string
}

variable "base_ami_image" {
  type = string
}

variable "proxy" {
  type = string
}

variable "no_proxy" {
  type = string
}

variable "ca_certificate" {
  type = string
}

variable "api_endpoint" {
  type = string
}

variable "vpc_cidr" {
  type = string
}