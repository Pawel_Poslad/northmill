resource "aws_eks_node_group" "node_group" {
  cluster_name    = var.cluster_name
  node_group_name = local.node_group_name
  node_role_arn   = var.worker_role_arn
  subnet_ids      = var.cluster_subnet_ids
  instance_types  = var.instance_types
  capacity_type   = var.capacity_type

  launch_template {
    name  = aws_launch_template.eks_launch_template.name
    version = aws_launch_template.eks_launch_template.latest_version
  }

  scaling_config {

    min_size     = var.nodes_min_count
    max_size     = var.nodes_max_count
    desired_size = var.nodes_desired_count
  }

  update_config {
    max_unavailable = 1
  }
}