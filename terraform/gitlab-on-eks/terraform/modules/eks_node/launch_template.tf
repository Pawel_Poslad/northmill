resource "aws_launch_template" "eks_launch_template" {
  name = local.launch_template_name
  image_id = var.base_ami_image

  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 1
  }

  user_data = base64encode(templatefile("${path.module}/user-data/userdata.tpl", {
    CLUSTER_NAME       = var.cluster_name,
    CLUSTER_NODE_GROUP = local.node_group_name,
    AMI_IMAGE          = var.base_ami_image,
    B64_CLUSTER_CA     = var.ca_certificate,
    API_SERVER_URL     = var.api_endpoint,
    proxy              = var.proxy,
    no_proxy           = var.no_proxy,
    VPC_CIDR           = var.vpc_cidr
  }))
    tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "EKS-MANAGED-NODE"
    }
  }
}