locals {
  tag_environment              = lookup(var.common_tags, "environment", "")
  node_group_name              = format("%s-%s", var.node_group_name, local.tag_environment)
  launch_template_name         = format("%s-%s", var.cluster_name, var.launch_template_name)
  account_id                   = data.aws_caller_identity.current.account_id
}