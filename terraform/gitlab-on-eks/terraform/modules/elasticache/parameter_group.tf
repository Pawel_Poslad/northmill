resource "aws_elasticache_parameter_group" "elasticache_parameter_group" {
  name   = format("elasticache-parametergroup-%s", local.tag_environment)
  family = "redis7"

  tags = merge(
    var.common_tags,
    {
      Name = format("elasticache-parametergroup-%s", local.tag_environment)
    },
  )
}