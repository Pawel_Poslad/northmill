variable "databases_subnets_ids" {
  type        = list(string)
  description = "Database subnet ids"
}

variable "common_tags" {
  type        = map(string)
  description = "Tags configurations"
}

variable "elasticache_node_type" {
  type        = string
  description = "Node type"
}

variable "elasticache_num_cache_nodes" {
  type        = number
  description = "Number of nodes"
}

variable "elasticache_port" {
  type        = number
  description = "Redis port"
  default     = 6379
}

variable "elasticache_engine" {
  type        = string
  description = "Elasticache version"
}

variable "elasticache_engine_version" {
  type        = string
  description = "Elasticache version"
}

variable "elasticache_sg_ids" {
  type        = list(string)
  description = "Elasticache security group ids (list)"
}