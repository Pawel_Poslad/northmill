output "elasticache_host" {
  value = aws_elasticache_cluster.gitlab.configuration_endpoint
}