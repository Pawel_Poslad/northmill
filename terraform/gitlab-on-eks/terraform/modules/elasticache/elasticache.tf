resource "aws_elasticache_cluster" "gitlab" {
  cluster_id           = format("gitlab-redis-%s", local.tag_environment)
  engine               = var.elasticache_engine
  node_type            = var.elasticache_node_type
  num_cache_nodes      = var.elasticache_num_cache_nodes
  parameter_group_name = aws_elasticache_parameter_group.elasticache_parameter_group.id
  engine_version       = var.elasticache_engine_version
  port                 = var.elasticache_port
  subnet_group_name    = aws_elasticache_subnet_group.elasticache_subnet_group.name
  security_group_ids   = var.elasticache_sg_ids
}