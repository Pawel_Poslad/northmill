resource "aws_elasticache_subnet_group" "elasticache_subnet_group" {
  name       = format("elasticache-subnetgroup-%s", local.tag_environment)
  subnet_ids = var.databases_subnets_ids

  tags = merge(
    var.common_tags,
    {
      Name = format("elasticache-subnetgroup-%s", local.tag_environment)
    },
  )
}