variable "databases_subnets_ids" {
  type        = list(string)
  description = "Database subnet ids"
}

variable "common_tags" {
  type        = map(string)
  description = "Tags configurations"
}

variable "rds_name" {
  type        = list
  description = "List of rds names"
}

variable "availability_zones" {
  type = number
}

variable "rds_allocated_storage" {
  type        = number
  description = "Allocated storage"
}

variable "rds_engine" {
  type        = string
  description = "Database engine"
}

variable "rds_engine_version" {
  type        = string
  description = "Database engine version"
}

variable "rds_db_type" {
  type        = string
  description = "Database type"
}

variable "rds_multi_az" {
  type        = bool
  description = "Multi AZ flag"
}

variable "rds_storage_type" {
  type        = string
  description = "Storage type"
}

variable "rds_skip_final_snapshot" {
  type        = bool
  description = "Skip final snapshot flag"
}

variable "rds_port" {
  type        = number
  description = "postgresql port"
  default     = 5432
}

variable "rds_apply_immediately" {
  type        = bool
  description = "Apply immediately flag"
}

variable "rds_enabled_cloudwatch_logs_exports" {
  type        = list(string)
  description = "Enabled cloudwatch logs (list)"
}

variable "rds_db_name" {
  type        = string
  description = "Database name"
}

variable "rds_username" {
  type        = string
  description = "Database username"
  default     = "domain_namenology_admin"
}

variable "rds_password_len" {
  type        = number
  description = "Password length"
  default     = 24
}

variable "rds_sg_ids" {
  type        = list(string)
  description = "Database security group ids (list)"
}

variable "rds_backup_retention_period" {
  type        = number
  description = "Backup retention period"
}

variable "rds_backup_window" {
  type        = string
  description = "Backup window"
}

variable "rds_encryption" {
  type        = bool
  description = "Encryption flag"
}