resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = format("subnetgroup-%s", local.tag_environment)
  subnet_ids = var.databases_subnets_ids

  tags = merge(
    var.common_tags,
    {
      Name = format("rds-subnetgroup-%s", local.tag_environment)
    },
  )
}