resource "random_password" "rds_password" {
  length  = var.rds_password_len
  special = false
}