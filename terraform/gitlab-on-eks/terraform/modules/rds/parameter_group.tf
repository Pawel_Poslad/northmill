resource "aws_db_parameter_group" "rds_parameter_group" {
  name   = format("parametergroup-%s", local.tag_environment)
  family = "postgres14"

  tags = merge(
    var.common_tags,
    {
      Name = format("rds-parametergroup-%s", local.tag_environment)
    },
  )
}