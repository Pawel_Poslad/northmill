resource "aws_db_instance" "gitlab-db" {
  for_each                        = toset(var.rds_name)
  identifier                      = format("%s-%s", each.key, (local.tag_environment))
  allocated_storage               = var.rds_allocated_storage
  engine                          = var.rds_engine
  engine_version                  = var.rds_engine_version
  instance_class                  = var.rds_db_type
  multi_az                        = var.rds_multi_az
  storage_type                    = var.rds_storage_type
  skip_final_snapshot             = var.rds_skip_final_snapshot
  port                            = var.rds_port
  apply_immediately               = var.rds_apply_immediately
  enabled_cloudwatch_logs_exports = var.rds_enabled_cloudwatch_logs_exports

  db_name  = var.rds_db_name
  username = var.rds_username
  password = random_password.rds_password.result

  vpc_security_group_ids = var.rds_sg_ids

  parameter_group_name = aws_db_parameter_group.rds_parameter_group.name

  backup_retention_period = var.rds_backup_retention_period
  backup_window           = var.rds_backup_window
  storage_encrypted       = var.rds_encryption
  db_subnet_group_name    = aws_db_subnet_group.rds_subnet_group.name

  tags = merge(
    var.common_tags,
    {
      Name = format("gitlab-rds-%s", local.tag_environment)
    },
  )
}