output "rds_password" {
  value = random_password.rds_password.result
}

output "rds_host" {
  value = aws_db_instance.gitlab-db
}