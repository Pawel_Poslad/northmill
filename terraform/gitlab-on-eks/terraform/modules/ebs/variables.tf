variable "common_tags" {
  type        = map(string)
  description = "tags configurations"
}

variable "availability_zone" {
  type = string
}

variable "ebs_size" {
  type = number
}

variable "ebs_type" {
  type = string
}

variable "ebs_encrypted" {
  type = bool
}

