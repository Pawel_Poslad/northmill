resource "aws_ebs_volume" "gitaly" {
  availability_zone = var.availability_zone
  size              = var.ebs_size
  type              = var.ebs_type
  encrypted         = var.ebs_encrypted

  tags = merge(
    var.common_tags,
    {
      Name = format("gitlab-%s", (local.tag_environment))
    }
  )
}