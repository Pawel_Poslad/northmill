resource "aws_route53_zone" "domain" {
  name = var.domain_name
  tags = var.common_tags

  vpc {
    vpc_id = var.vpc_id
  }
}

resource "aws_route53_record" "gitlab" {
 zone_id    = aws_route53_zone.domain.zone_id
 name       = "gitlab.${var.domain_name}"
 type       = "CNAME"
 ttl        = "300"
 records    = [data.kubernetes_service.gitlab-webservice.status.0.load_balancer.0.ingress.0.hostname]

 depends_on = [
   helm_release.gitlab,
   data.kubernetes_service.gitlab-webservice
 ]
}