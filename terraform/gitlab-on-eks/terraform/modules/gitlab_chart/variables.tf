variable "common_tags" {
  type        = map(string)
  description = "tags configurations"
}

variable "vpc_id" {
  type = string
}

variable "domain_name" {
  type        = string
  default     = "domain-name.aws_region.aws.com"
  description = "domain name"
}

variable "gitlab_dns_name" {
  type = string
}

variable "gitlab_access_role_arn" {
  type = string
}

variable "rds_host_primary" {
  type = string
}

variable "rds_host_replica_0" {
  type = string
}

variable "rds_port" {
  type = number
}

variable "rds_password" {
  type = string
}

variable "region" {
  type = string
}

variable "s3_gitlab_registry_id" {
  type = string
}

variable "s3_gitlab_backups_id" {
  type = string
}

variable "s3_git_lfs_id" {
  type = string
}

variable "s3_gitlab_artifacts_id" {
  type = string
}

variable "s3_gitlab_uploads_id" {
  type = string
}

variable "s3_gitlab_packages_id" {
  type = string
}

variable "s3_gitlab_pseudo_id" {
  type = string
}

variable "gitaly_ebs_volume_id" {
  type = string
}

variable "availability_zones" {
  type = list
}

variable "elasticache_host" {
  type = string
}

variable "certmanager_issuer_email" {
  type = string
  default = "pawel.poslad@kuehne-nagel.com"
}

variable "acm_cert" {
  type = string
  default = "arn:aws:acm:aws_region:955962105480:certificate/1bd1be89-ddb3-4533-8816-4674e075f920"
}