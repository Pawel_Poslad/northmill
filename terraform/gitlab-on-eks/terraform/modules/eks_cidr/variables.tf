variable "common_tags" {
  type        = map(string)
  description = "tags configurations"
}

variable "eks_az1" {
  type = string
}

variable "eks_az2" {
  type = string
}

variable "eks_az3" {
  type = string
}

variable "eks_cidr_subnet_id1" {
  type = string
}

variable "eks_cidr_subnet_id2" {
  type = string
}

variable "eks_cidr_subnet_id3" {
  type = string
}

variable "eks_cluster_name" {
  type = string
}
