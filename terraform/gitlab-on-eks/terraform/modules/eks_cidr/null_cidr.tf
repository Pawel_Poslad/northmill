resource "null_resource" "cidr" {
triggers = {
    always_run = timestamp()
}
provisioner "local-exec" {
    on_failure  = fail
    when = create
    interpreter = ["/bin/bash", "-c"]
    command     = <<EOT
        az1=$(echo ${var.eks_az1})
        az2=$(echo ${var.eks_az2})
        az3=$(echo ${var.eks_az3})
        sub1=$(echo ${var.eks_cidr_subnet_id1})
        sub2=$(echo ${var.eks_cidr_subnet_id2})
        sub3=$(echo ${var.eks_cidr_subnet_id3})
        cn=$(echo ${var.eks_cluster_name})
        echo $az1 $az2 $az3 $sub1 $sub2 $sub3 $cn
        echo -e "\x1B[35mCycle nodes for custom CNI setting (takes a few minutes) ......\x1B[0m"
        /Users/pawel.poslad/repos/IaC/terraform/modules/eks_cidr/cni-cycle-nodes.sh $cn
        echo -e "\x1B[33mAnnotate nodes ......\x1B[0m"
        /Users/pawel.poslad/repos/IaC/terraform/modules/eks_cidr/annotate-nodes.sh $az1 $az2 $az3 $sub1 $sub2 $sub3 $cn
        echo -e "\x1B[32mShould see coredns on 100.64.x.y addresses ......\x1B[0m"
        echo -e "\x1B[32mkubectl get pods -A -o wide | grep coredns\x1B[0m"
     EOT
    }
}