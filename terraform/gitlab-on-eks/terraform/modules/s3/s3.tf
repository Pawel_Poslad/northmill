resource "aws_s3_bucket" "s3_bucket" {
  for_each      = toset(var.s3_name)
  bucket        = format("%s-%s", each.key, (local.tag_environment))
  force_destroy = var.s3_force_destroy


  tags = merge(
    var.common_tags,
    {
      Name = format("gitlab-%s", (local.tag_environment))
    }
  )
}

resource "aws_s3_bucket_acl" "s3_bucket_acl" {
  for_each = toset(var.s3_name)
  bucket   = aws_s3_bucket.s3_bucket[each.key].id
  acl      = var.s3_acl
}

resource "aws_s3_bucket_public_access_block" "s3_bucket_public_access" {
  for_each = toset(var.s3_name)
  bucket   = aws_s3_bucket.s3_bucket[each.key].id

  block_public_acls       = var.s3_block_public_acls
  block_public_policy     = var.s3_block_public_policy
  ignore_public_acls      = var.s3_ignore_public_acls
  restrict_public_buckets = var.s3_restrict_public_buckets
}

resource "aws_s3_bucket_server_side_encryption_configuration" "s3_bucket_server_side_encryption_configuration" {
  for_each = toset(var.s3_name)
  bucket   = aws_s3_bucket.s3_bucket[each.key].id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "AES256"
    }
  }
}
