variable "common_tags" {
  type        = map(string)
  description = "Tags configurations"
}

variable "s3_name" {
  type        = list
  description = "List with names of s3 bucket"
}

variable "s3_acl" {
  type        = string
  description = "Bucket permission"
  default     = "private"
}

variable "s3_force_destroy" {
  type        = bool
  description = "Force destroy flag"
  default     = false
}

variable "s3_block_public_acls" {
  type        = bool
  description = "Block public acls flag"
  default     = true
}

variable "s3_block_public_policy" {
  type        = bool
  description = "Block public policy flag"
  default     = true
}

variable "s3_ignore_public_acls" {
  type        = bool
  description = "Ignore public acls flag"
  default     = true
}

variable "s3_restrict_public_buckets" {
  type        = bool
  description = "Restrict publicn buckets flag"
  default     = true
}