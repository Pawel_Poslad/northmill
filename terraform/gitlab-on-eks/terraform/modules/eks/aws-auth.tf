resource "kubernetes_config_map" "aws_auth_configmap" {
  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
  }
  data = {
    "mapRoles" = templatefile("${path.module}/configmap/aws-auth.yaml", {account_id = local.account_id, env = local.tag_environment})
  }
}