data "aws_iam_policy" "AmazonEKS_CNI_Policy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}

resource "aws_iam_role" "eks_cni_service_role" {
  assume_role_policy = data.aws_iam_policy_document.oidc_assume_role_policy.json
  name               = "${local.cluster_name}-cni-servicerole"
}

resource "aws_iam_role_policy_attachment" "eks_cni_service_role_policy" {
  role       = aws_iam_role.eks_cni_service_role.name
  policy_arn = data.aws_iam_policy.AmazonEKS_CNI_Policy.arn
}