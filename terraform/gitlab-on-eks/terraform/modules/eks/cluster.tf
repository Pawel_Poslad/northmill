resource "aws_eks_cluster" "eks" {
  name     = local.cluster_name
  role_arn = var.cluster_role_arn
  version  = var.cluster_version

  vpc_config {
    subnet_ids              = var.cluster_subnet_ids
    endpoint_private_access = var.endpoint_private_access
    endpoint_public_access  = var.endpoint_public_access
    security_group_ids      = var.security_group_ids
  }

  depends_on = [aws_cloudwatch_log_group.eks]

  enabled_cluster_log_types = var.cluster_log_types

}

data "aws_eks_cluster_auth" "eks" {
  name = aws_eks_cluster.eks.name
}