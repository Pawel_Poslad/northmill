resource "aws_cloudwatch_log_group" "eks" {
  name              = "/aws/eks/${local.cluster_name}/cluster"
  retention_in_days = var.cloudwatch_logs_retention

  tags = merge(
    {
      "Name" = "/aws/eks/${local.cluster_name}/cluster",
    },
  var.common_tags)
}

resource "aws_cloudwatch_log_group" "eks_workers" {
  name              = "/aws/eks/${local.cluster_name}/instances"
  retention_in_days = var.cloudwatch_logs_retention

  tags = merge(
    {
      "Name" = "/aws/eks/${local.cluster_name}/instances",
    },
  var.common_tags)
}
