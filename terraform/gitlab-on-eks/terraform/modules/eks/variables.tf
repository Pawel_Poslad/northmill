variable "cluster_name" {
  type        = string
  description = "EKS cluster name"
}

variable "cluster_role_arn" {
  type        = string
  description = "EKS cluster role ARN"
}

variable "cluster_version" {
  type        = string
  description = "EKS cluster version"
}

variable "cluster_subnet_ids" {
  type        = list(any)
  description = "List of subnets to EKS cluster to operate"
}

variable "endpoint_private_access" {
  type        = bool
  default     = true
  description = "EKS private API"
}

variable "endpoint_public_access" {
  type        = bool
  default     = true
  description = "EKS public API"
}

variable "cloudwatch_logs_retention" {
  type        = number
  default     = 7
  description = "EKS cluster logs retention"
}

variable "common_tags" {
  type        = map(any)
  default     = {}
  description = "Tags map"
}

variable "security_group_ids" {
  type        = list(any)
  default     = []
  description = "EKS cluster additional security groups if any"
}

variable "cluster_log_types" {
  type    = list(any)
  default = [] # ["api", "audit"]
}