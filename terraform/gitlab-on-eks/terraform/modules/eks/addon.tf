resource "aws_eks_addon" "aws_ebs_csi_driver" {
  cluster_name             = local.cluster_name
  resolve_conflicts        = "OVERWRITE"
  addon_name               = "aws-ebs-csi-driver"
  service_account_role_arn = aws_iam_role.ebs_csi_service_role.arn
  }