output "name" {
  description = "EKS cluster name"
  value       = aws_eks_cluster.eks.name
}

output "arn" {
  description = "EKS cluster name"
  value       = aws_eks_cluster.eks.arn
}

output "cluster_sg_id" {
  description = "EKS cluster created main SG id"
  value       = aws_eks_cluster.eks.vpc_config[0].cluster_security_group_id
}

output "ca_certificate" {
  description = "EKS cluster CA Certificate (Base64)"
  value       = aws_eks_cluster.eks.certificate_authority[0].data
}

output "endpoint" {
  description = "EKS cluster endpoint"
  value       = aws_eks_cluster.eks.endpoint
}

output "token" {
  description = "EKS cluster token for kubernetes provider (refreshed every time)"
  value       = data.aws_eks_cluster_auth.eks.token
}

output "oidc_eks_arn" {
  value = aws_iam_openid_connect_provider.eks.arn
}

output "oidc_eks_url" {
  value = aws_iam_openid_connect_provider.eks.url
}

output "cni_role_arn" {
  value = aws_iam_role.eks_cni_service_role.arn
}

output "gitlab_access_role_arn" {
  value = aws_iam_role.gitlab_access.arn
}

output "cni_role_annotation" {
  value = "kubectl annotate serviceaccount -n kube-system aws-node eks.amazonaws.com/role-arn=${aws_iam_role.eks_cni_service_role.arn}"
}
