data "aws_iam_policy" "AmazonEKSClusterPolicy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

data "aws_iam_policy" "AmazonEKSVPCResourceController" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
}

data "aws_iam_policy_document" "eks_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }

    effect = "Allow"
  }
}

resource "aws_iam_role" "eks_role" {
  name               = "${local.cluster_name}-role"
  assume_role_policy = data.aws_iam_policy_document.eks_assume_role_policy.json
  tags               = var.common_tags
}

resource "aws_iam_role_policy_attachment" "eks_role_cluster_policy" {
  role       = aws_iam_role.eks_role.name
  policy_arn = data.aws_iam_policy.AmazonEKSClusterPolicy.arn
}

resource "aws_iam_role_policy_attachment" "eks_vpc_controller_service_policy" {
  role       = aws_iam_role.eks_role.name
  policy_arn = data.aws_iam_policy.AmazonEKSVPCResourceController.arn
}

resource "aws_iam_role_policy_attachment" "eks_role_cw_logs_policy" {
  role       = aws_iam_role.eks_role.name
  policy_arn = aws_iam_policy.cw_logs_writer.arn
}

resource "aws_iam_role_policy_attachment" "eks_cluster_kms_permissions" {
  role       = aws_iam_role.eks_role.name
  policy_arn = aws_iam_policy.eks_cluster_kms_permissions.arn
}