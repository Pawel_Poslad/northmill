variable "cluster_name" {
  type = string
}

variable "common_tags" {
  type    = map(any)
  default = {}
}

variable "ssm_key_arn" {
  type    = string
  default = "*"
}

variable "kms_key_arn" {
  type        = string
  description = "KMS key for nodes"
}

variable "region" {
  type = string
}