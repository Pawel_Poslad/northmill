data "aws_iam_policy_document" "eks_cw_logs_policy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]

    resources = [
      "arn:aws:logs:*:*:*"
    ]

    effect = "Allow"
  }
}

resource "aws_iam_policy" "cw_logs_writer" {
  name        = "${local.cluster_name}-cwlogs-writer"
  description = "CW Logs Writing Policy"
  policy      = data.aws_iam_policy_document.eks_cw_logs_policy.json
}

data "aws_iam_policy_document" "sts_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    resources = ["*"]
  }
}

resource "aws_iam_policy" "allow_all_sts" {
  name   = "${local.cluster_name}-assume-sts"
  policy = data.aws_iam_policy_document.sts_assume_role_policy.json
}

data "aws_iam_policy_document" "eks_ssm_policy" {
  statement {
    actions = [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel",
      "s3:GetEncryptionConfiguration",
      "ssm:RunCommand",
    ]

    resources = [
      "*"
    ]

    effect = "Allow"
  }
  statement {
    actions = [
      "kms:Decrypt"
    ]

    resources = [
      var.ssm_key_arn
    ]

    effect = "Allow"
  }
}

resource "aws_iam_policy" "eks_ssm" {
  description = "EKS-SSM Policy"
  name        = "${local.cluster_name}-ssm"
  policy      = data.aws_iam_policy_document.eks_ssm_policy.json
}

data "aws_iam_policy_document" "eks_efs_policy" {
  statement {
    actions = [
      "elasticfilesystem:DescribeAccessPoints",
      "elasticfilesystem:DescribeFileSystems"
    ]

    resources = [
      "*"
    ]

    effect = "Allow"
  }

  statement {
    actions = [
      "elasticfilesystem:CreateAccessPoint"
    ]

    resources = [
      "*"
    ]

    condition {
      test     = "StringLike"
      variable = "aws:RequestTag/efs.csi.aws.com/cluster"

      values = [
        "true"
      ]
    }

    effect = "Allow"
  }

  statement {
    actions = [
      "elasticfilesystem:DeleteAccessPoint"
    ]

    resources = [
      "*"
    ]

    condition {
      test     = "StringEquals"
      variable = "aws:RequestTag/efs.csi.aws.com/cluster"

      values = [
        "true"
      ]
    }

    effect = "Allow"
  }
}



resource "aws_iam_policy" "eks_efs" {
  description = "EKS EFS Policy"
  name        = "${local.cluster_name}-efs"
  policy      = data.aws_iam_policy_document.eks_efs_policy.json
}

data "aws_iam_policy_document" "eks_cluster_kms_permissions" {
  statement {
    actions = [
      "kms:*"
    ]

    resources = [
      var.kms_key_arn
    ]

    effect = "Allow"
  }
}

resource "aws_iam_policy" "eks_cluster_kms_permissions" {
  name        = "${local.cluster_name}-kms-volumes"
  description = "KMS for EKS volumes policy"
  policy      = data.aws_iam_policy_document.eks_cluster_kms_permissions.json
}