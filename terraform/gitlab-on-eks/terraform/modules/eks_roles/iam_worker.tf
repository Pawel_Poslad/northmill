data "aws_iam_policy_document" "worker_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "worker" {
  name               = "${local.cluster_name}-worker-role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.worker_assume_role_policy.json
  tags               = var.common_tags
}

resource "aws_iam_role_policy_attachment" "worker_eks_worker_node_policy" {
  role       = aws_iam_role.worker.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

resource "aws_iam_role_policy_attachment" "worker_eks_cni_policy" {
  role       = aws_iam_role.worker.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}

resource "aws_iam_role_policy_attachment" "worker_ec2_container_registry_read_only" {
  role       = aws_iam_role.worker.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "worker_role_cw_logs_policy" {
  role       = aws_iam_role.worker.name
  policy_arn = aws_iam_policy.cw_logs_writer.arn
}

resource "aws_iam_role_policy_attachment" "worker_allow_all_sts" {
  role       = aws_iam_role.worker.name
  policy_arn = aws_iam_policy.allow_all_sts.arn
}

resource "aws_iam_role_policy_attachment" "worker_ssm_policy" {
  role       = aws_iam_role.worker.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "worker_ssm_sessions_policy" {
  role       = aws_iam_role.worker.name
  policy_arn = aws_iam_policy.eks_ssm.arn
}

resource "aws_iam_role_policy_attachment" "worker_efs_access_policy" {
  role       = aws_iam_role.worker.name
  policy_arn = aws_iam_policy.eks_efs.arn
}

resource "aws_iam_role_policy_attachment" "worker_kms_policy" {
  role       = aws_iam_role.worker.name
  policy_arn = aws_iam_policy.eks_cluster_kms_permissions.arn
}