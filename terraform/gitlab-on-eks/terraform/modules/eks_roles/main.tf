resource "aws_iam_instance_profile" "instance_profile" {
  name = "${local.cluster_name}-worker-profile"
  role = aws_iam_role.worker.name
  tags = var.common_tags
}