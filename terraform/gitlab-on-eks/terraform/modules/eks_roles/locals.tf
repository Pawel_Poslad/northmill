locals {
  tag_environment = lookup(var.common_tags, "environment", "")
  cluster_name    = format("%s-%s", var.cluster_name, local.tag_environment)
  aws_account_id  = data.aws_caller_identity.current.account_id
}